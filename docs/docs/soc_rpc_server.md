Functions for running an RPC server on the ZCU216 to allow users to submit remote jobs/circuits. See the [Getting Started Guide](https://gitlab.com/LBL-QubiC/software/-/wikis/Getting-Started-with-QubiC-2.0-on-the-ZCU216) for details on how to configure.

::: qubic.soc_rpc_server
