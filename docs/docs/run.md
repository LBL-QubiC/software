AbstractCircuitRunner implementation which runs locally on the ZCU216. Used by soc_rpc_server to load and run circuits. Can also be used locally, if user environment is on the ZCU216.

::: qubic.run
